// pages/index/index.js
// 引入three
import * as THREE from '../../js/three.weapp.js'
// 引入GLTFLoader
import {
  registerGLTFLoader
} from '../../js/GLTFloader.js'
// 引入GLTFLoader
import {
  registerFBXLoader
} from '../../js/FBXLoader.js'
import registerOrbit from '../../js/OrbitControls'
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 初始化模型
    wx.createSelectorQuery()
      .select("#webgl")
      .node()
      .exec(res => {
        const canvas = THREE.global.registerCanvas(res[0].node)
        // 注册loader  这里使用了FBX  和GLTF  两种格式的模型
        registerGLTFLoader(THREE)
        registerFBXLoader(THREE)
        // 创建场景
        let scene = new THREE.Scene()
        // 创建相机
        const camera = new THREE.PerspectiveCamera(75, canvas.width / canvas.height, 0.25, 100);
        camera.position.set(0, 10, 20);
        camera.lookAt(new THREE.Vector3(0, 0, 0))
        // 灯光
        scene.add(new THREE.AmbientLight(0xffffff, 1))
        scene.add(new THREE.HemisphereLight(0xffffff, 0x444444))
        // renderer
        const renderer = new THREE.WebGLRenderer({
          antialias: true, //平滑效果
          alpha: true //背景透明
        });
        wx.getSystemInfo({
          success: function (res) {
            renderer.setPixelRatio(res.devicePixelRatio)
          }
        })
        // 创建时钟
        let clock = new THREE.Clock()
        // 注册 OrbitControls
        const {
          OrbitControls
        } = registerOrbit(THREE)
        let controls = new OrbitControls(camera, renderer.domElement);
        controls.update()
        // GLTFLoader
        let GLTFLoader = new THREE.GLTFLoader()
        GLTFLoader.load('http://www.webgl3d.cn/threejs/examples/models/gltf/RobotExpressive/RobotExpressive.glb', function (gltf) {
          let model = gltf.scene
          scene.add(model)
          let mixer = new THREE.AnimationMixer(model)
          let AnimationAction = mixer.clipAction(gltf.animations[1])
          AnimationAction.play()
        })
        // FBXLoader
        let FBXLoader = new THREE.FBXLoader()
        // FBXLoader写法大同小异
        // animate
        function animate() {
          canvas.requestAnimationFrame(animate)
          let delta = clock.getDelta()
          controls.update(delta)
          renderer.render(scene, camera)
        }
        animate()
      })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  touchStart(e) {
    THREE.global.touchEventHandlerFactory('canvas', 'touchstart')(e)
  },
  touchMove(e) {
    THREE.global.touchEventHandlerFactory('canvas', 'touchmove')(e)
  },
  touchEnd(e) {
    THREE.global.touchEventHandlerFactory('canvas', 'touchend')(e)
  },
})